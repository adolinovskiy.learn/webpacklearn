import _ from 'lodash';
import './style.css';
// import Icon from './icon.png';
// https://webpack.js.org/guides/output-management/#setting-up-htmlwebpackplugin
// https://habr.com/ru/articles/701724/#%D0%BF%D0%BE%D0%B4%D0%BA%D0%BB%D1%8E%D1%87%D0%B5%D0%BD%D0%B8%D0%B5-%D1%88%D1%80%D0%B8%D1%84%D1%82%D0%BE%D0%B2
import printMe from './print.js';
function component() {
    const element = document.createElement('div');
    const btn = document.createElement('button');

    // Lodash, currently included via a script, is required for this line to work
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.classList.add('hello');

    btn.innerHTML = 'Click me and check the console!';
    btn.onclick = printMe;
    element.appendChild(btn);
    // const myIcon = new Image();
    // myIcon.src = Icon;
    //
    // element.appendChild(myIcon);
    return element;
}

document.body.appendChild(component());